```
#!xml
<repositories>
 <repository>
  <id>bitbucket-release</id>
  <url>https://bitbucket.org/samuelventura/mavenrepo/raw/master/releases/</url>
 </repository>
 <repository>
  <id>bitbucket-snapshot</id>
  <url>https://bitbucket.org/samuelventura/mavenrepo/raw/master/snapshots/</url>
 </repository>
</repositories>
```
* mvn -Dmaven.repo.local=../../mavenrepo/snapshots install
* mvn -Dmaven.repo.local=../../mavenrepo/releases install